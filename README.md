# bem-classname

Implementation of [bem-react/classname](https://ru.bem.info/technologies/bem-react/classname/) in php

Usage:
```php
$preset = new BemPreset();
$factory = new BemFactory($preset);
$block = $factory->block('test');

$block->cn(['color' => 'yellow', 'active']); // test test_color_yellow test_active

$block->element('elem', ['type' => 'button', 'active']); // test__elem test__elem_button_yellow __elem_active
```
