<?php

namespace Upline\BemClassname;

class BemBlock
{
    /**
     * @var BemPreset
     */
    private $bemPreset;
    /**
     * @var string
     */
    private $blockName;

    public function __construct(BemPreset $bemPreset, string $name)
    {
        $this->bemPreset = $bemPreset;
        $this->blockName = $name;
    }

    public function cn(array $modifiers = [], array $mix = []): string
    {
        return $this->processModifiers($this->blockName, $modifiers, $mix);
    }

    public function element(string $name, array $modifiers = [], array $mix = []): string
    {
        return $this->processModifiers(
            $this->blockName . $this->bemPreset->getElementDelimiter() . $name,
            $modifiers,
            $mix
        );
    }

    private function processModifiers(string $currentName, array $modifiers = [], array $mix = []): string
    {
        $className = $this->bemPreset->getNamespace() . $currentName;
        $classes = [
            $className
        ];

        foreach ($modifiers as $key => $value) {
            if (is_integer($key)) {
                $classes[] = $className . $this->bemPreset->getModifierDelimiter() . $value;
            } else {
                $classes[] = $className . $this->bemPreset->getModifierDelimiter() . $key . $this->bemPreset->getModifierValueDelimiter() . $value;
            }
        }

        array_push($classes, ...$mix);

        return join(' ', $classes);
    }
}
