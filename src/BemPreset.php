<?php

namespace Upline\BemClassname;

class BemPreset
{
    /**
     * @var string
     */
    private $elementDelimiter;
    /**
     * @var string
     */
    private $modifierDelimiter;
    /**
     * @var string
     */
    private $modifierValueDelimiter;
    /**
     * @var string
     */
    private $namespace;

    public function __construct(
        string $elementDelimiter = '__',
        string $modifierDelimiter = '_',
        string $modifierValueDelimiter = '_',
        string $namespace = ''
    ) {
        $this->elementDelimiter = $elementDelimiter;
        $this->modifierDelimiter = $modifierDelimiter;
        $this->modifierValueDelimiter = $modifierValueDelimiter;
        $this->namespace = $namespace;
    }

    /**
     * @return string
     */
    public function getElementDelimiter(): string
    {
        return $this->elementDelimiter;
    }

    /**
     * @return string
     */
    public function getModifierDelimiter(): string
    {
        return $this->modifierDelimiter;
    }

    /**
     * @return string
     */
    public function getModifierValueDelimiter(): string
    {
        return $this->modifierValueDelimiter;
    }

    /**
     * @return string
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }
}
