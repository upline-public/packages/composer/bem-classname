<?php

namespace Upline\BemClassname;

class BemFactory
{
    /**
     * @var BemPreset
     */
    private $bemPreset;

    public function __construct(BemPreset $bemPreset)
    {
        $this->bemPreset = $bemPreset;
    }

    public function block(string $name): BemBlock
    {
        return new BemBlock($this->bemPreset, $name);
    }
}
